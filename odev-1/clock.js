let days = ["Sunday", "Monday", "Tuesday", "Wednesday", "Thursday", "Friday", "Saturday"];
let getName = prompt("What is your name?");
let myName = document.querySelector("#myName");

if(getName){
    myName.innerHTML = `${getName}`;
}else{
    alert("Please enter a name!");
    location.reload();
}

function clockFunction(){
    let dateTime = new Date();
    let day = days[dateTime.getDay()];
    let hours = days[dateTime.getHours()];
    let minutes = days[dateTime.getMinutes()];
    let seconds = days[dateTime.getSeconds()];

    document.querySelector("#myClock").innerHTML = `${hours} : ${minutes} : ${seconds} - ${day}`;
    

    setInterval(clockFunction, 1000);
}
